const path = require("path");
const {Sequelize}= require("sequelize")
const sequelize = require(path.join(__dirname,'./data/db'));
const Task  = require(path.join(__dirname,"./models/Task"))(sequelize, Sequelize.DataTypes);

class TaskService {
  async inTransaction(work) {
    const t = await sequelize.transaction();
    try {
      await work(t);
      return t.commit();
    } catch (error) {
      t.rollback();
      throw error;
    }
}

  static async getList(userID) {
      return Task.findAll({
          where:{
            userID:userID,
            done:false,
          }
      })
  }

    static async getDoneList(userID){
        return Task.findAll({
          where:{
            userID:userID,
            done:true,
          }
        })
    }

    static async markDone( userID, taskName) {
        const marked = await Task.update({done:true},{
          where:{
            userID:userID,
            taskName:taskName,
          }
        })

        if(marked[0]===0){
          throw new Error("Error");
        }

        return await Task.findOne({
          where:{
            userID:userID,
            taskName:taskName,
            done:true
          }
        })
  }
    

    static async addEntry(userID, taskName, taskDescription, t) {
      const exists = await Task.findOne({
        where: {
          userID: userID,
          taskName: taskName
        }
      });
    
      if (exists !== null) {
        throw new Error("Task already exists.");
      }
    
      return await Task.create({
        userID: userID,
        taskName: taskName,
        taskDescription: taskDescription
      }, {
        transaction: t
      });
  }
    
    static async deleteTask(userID, taskName, t){
      const deleted = await Task.destroy({
        where:{
          userID:userID,
          taskName:taskName,
        }
      },{
        transaction:t
      })

      if (deleted === 0) {
        throw new Error("No task was deleted because it does not exist.");
      }

      return deleted;
    }

    static async changeTask(userID, taskName, taskDescription){    
      const changed = await Task.update({taskDescription:taskDescription},{
        where:{
          userID:userID,
          taskName:taskName,
        }
      })

      if(changed[0]===0){
        throw new Error("no changed object")
      }

      return await Task.findOne({
        where:{
          userID:userID,
          taskName:taskName,
          taskDescription:taskDescription
        }
      });
    }
}

module.exports = TaskService;