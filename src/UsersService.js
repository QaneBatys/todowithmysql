const path = require("path");
const {Sequelize}= require("sequelize")
const sequelize = require(path.join(__dirname,'./data/db'));
const User  = require(path.join(__dirname,"./models/User"))(sequelize, Sequelize.DataTypes);

class UsersService {
    async inTransaction(work) {
        const t = await this.client.transaction();
        try {
          await work(t);
          return t.commit();
        } catch (error) {
          t.rollback();
          throw error;
        }
    }

    static async addEntry(email, password, firstName , lastName,t  ){
const exists = await Task.findOne({
    where: {
      userID: userID,
      taskName: taskName
    }
  });

  if (exists !== null) {
    throw new Error("Task already exists.");
  }

  return await Task.create({
    userID: userID,
    taskName: taskName,
    taskDescription: taskDescription
  }, {
    transaction: t
  });
    }

    static async getList() {
        return User.findAll({});
    }  


    static async loggingIn(email, password){
        const logged = await User.findOne({
            where:{
                email:email, 
                password:password
            }
        })

        if(!logged){
            throw new Error("no user");
        }

        return logged.id;
    }
}

module.exports = UsersService;