const { Model } = require("sequelize");

module.exports = (sequelize, DataTypes) => {
  class Task extends Model {}
  Task.init(
    {
      userID: {
        type:DataTypes.INTEGER,
        allowNull: false,
        references: {
            model: 'Users', // name of the Users table
            key: 'id', // key in the Users table that we're referencing
        },
      },
      taskName: {
        type:DataTypes.STRING,
        unique:true,
      },
      taskDescription: DataTypes.STRING,
      done:{ 
        type:DataTypes.BOOLEAN,
        defaultValue: false
    }
    },
    {
      sequelize,
      modelName: "Task",
      timestamps: false,
    }
  );

  Task.associate = function(models) {
    // Assuming a User has many Tasks
    Task.belongsTo(models.User, { foreignKey: 'userID', as: 'user' });
  };

  sequelize.sync();
  return Task;
};