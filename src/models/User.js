const { Model } = require("sequelize");

module.exports = (sequelize, DataTypes) => {
  class User extends Model {}
  User.init(
    {
      email: {
        type:DataTypes.STRING,
        unique:true,
      },
      password: DataTypes.STRING,
      firstName: DataTypes.STRING,
      secondName: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "User",
      timestamps: false,
    }
  );

  sequelize.sync();
  return User;
};