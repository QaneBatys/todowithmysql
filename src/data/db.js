const { Sequelize } = require('sequelize');

const sequelize = new Sequelize('taskManager', 'root', 'mypassword', {
    host: 'localhost',
    port:3406,
    dialect: "mysql"
});

module.exports = sequelize;