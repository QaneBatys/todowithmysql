const express = require("express");
const router = express.Router();
const doneRoute = require("./done");

module.exports = (params) => {
  const { tasksService } = params;

  router.get("/", async (req, res) => {
    const tasks = await tasksService.getList();
    res.render("layout", {
      pageTitle: "Done",
      template: "done",
      tasks
    });
  });

  //router.get("/api/:taskName",)


  

  return router;
};
